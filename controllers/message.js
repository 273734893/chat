//聊天列表	
var MessageModel = require('../models/message');
//读取
exports.list = function(req, res) {
    MessageModel
        .find({
            room_id: req.params.room_id
        })
        .sort(req.params.sort ? '-_id' : '_id')
        .skip(req.params.offset)
        .limit(req.params.num)
        .exec(function(err, result) {
            if (!err && result) {
            	if(!req.params.isold) result.reverse();
                res.json({
                    result: 1,
                    data: result
                });
            } else {
                console.log(err);
                res.json({
                    result: 0
                });
            }
        });
};
