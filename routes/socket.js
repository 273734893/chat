module.exports = function(app, models) {
    var io = require('socket.io').listen(app.server);
    var cookie = require('express/node_modules/cookie');
    var _ = require('underscore');
    var UserModel = require('../models/user');
    var MessageModel = require('../models/message');

    // io.set('authorization', function(data, callback){
    //     var dataCookie = cookie.parse(data.headers.cookie);
    //     var connect_sid = dataCookie['connect.sid'];
    //     if (connect_sid) {
    //         var sid = connect_sid.split(':')[1].split('.')[0];
    //         app.sessionStore.get(sid, function(error, session){
    //             // console.warn(sid);
    //             if (error || !session) {
    //                 callback(error.message, false);
    //             }else {
    //                 data.session = session;
    //                 callback(null, true);
    //             }
    //         });
    //     }else {
    //         callback('nosession');
    //     }
    // });

    app.get('/online/:uid/:my_uid/:roomid', function(req, res) {
        UserModel.findOne({
            uid: req.params.uid,
        }, function(err, result) {
            if (!err && result) {
                UserModel.findOne({
                    uid: req.params.my_uid,
                }, function(err2, result2) {
                    if (!err2 && result2) {
                        var hasUnread = _.findWhere(result2.rooms, {room_id: req.params.roomid});
                        res.json({
                            result: 1,
                            data: {
                                online: result.online,
                                unread_num: hasUnread ? hasUnread.unread_num : 0
                            }
                        });
                    } else {
                        res.json({
                            result: 0
                        });
                    }
                });
            } else {
                res.json({
                    result: 0
                });
            }
        });
    });

    var chat = io.of('/chat').on('connection', function(socket) {
        var setRooms = function(options, action){
            var theRoom = {
                uid: 0, 
                to_uid: 0, 
                room_id: '',  
                open_rooms: []
            };
            if(!options) return false;
            _.extend(theRoom, options);
            UserModel.findOne({
                uid: theRoom.uid,
            }, function(err, result) {
                if (!err && result) {
                    if(!_.isEmpty(theRoom.open_rooms)) result.open_rooms = theRoom.open_rooms;
                    var hasUnread = _.findWhere(result.rooms, {room_id: theRoom.room_id}) || {};
                    if(hasUnread){
                        hasUnread.unread_num = (action == 'addNum') ? hasUnread.unread_num + 1 : 0;
                    }else{
                        hasUnread.unread_num = (action == 'addNum') ? 1 : 0;
                        result.rooms.push({
                            room_id: theRoom.room_id,
                            unread_num: hasUnread.unread_num
                        });
                    }
                    result.save(function(err, ok){
                        if(err){
                            console.log('Somthing wrong: ' + err);
                        }else{
                            console.warn('init');            
                        }
                    });
                    socket.broadcast.emit('has_message', {
                        uid: theRoom.to_uid,
                        to_uid: theRoom.uid,
                        unread_num: hasUnread.unread_num
                    });
                }
            });
        };
        var setOpenRooms = function(options){
            var defaults = {
                uid: 0,  
                open_rooms: []
            };
            if(!options) return false;
            _.extend(defaults, options);
            UserModel.update({
                uid: defaults.uid
            }, {
                open_rooms: defaults.open_rooms
            }, function(err, result) {
                if (!err && result) {
                    console.info('setOpenRooms');  
                }
            });
        };
        var isShowTime = function(earlyTime){
            var currentTime = (new Date()).getTime();
            var theMinutes = (currentTime - earlyTime || currentTime)/(1000 * 60);
            console.warn(theMinutes);
            return theMinutes >= 1 ? true : false;
        };
        var guid = function() {
            function S4() {
                return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            }
            return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
        };
        // 进入房间
        socket.on('join_room', function(data) {
            socket.join(data.room_id);
            console.warn('enter:', data.room_id);
            setRooms({
                uid: data.uid, 
                room_id: data.room_id, 
                open_rooms: data.open_rooms
            }, 'clearNum');
            console.warn(data.open_rooms);
            setOpenRooms({
                uid: data.uid,
                open_rooms: data.open_rooms
            });
        });
        // 退出房间
        socket.on('exit_room', function(data) {
            setOpenRooms({
                uid: data.uid,
                open_rooms: data.open_rooms
            });
        });
        // 发送消息
        socket.on('new_message', function(data) {
            UserModel.findOne({
                uid: data.to_uid
            }, function(err, result) {
                var early_time = 0;
                if(result){
                    var theMessage = _.findWhere(result.rooms, {room_id: data.room_id});
                    if(theMessage){
                        if(theMessage.early_time) early_time = theMessage.early_time;
                        theMessage.early_time = (new Date()).getTime();
                        result.save(function(err, ok){
                            if(err){
                                console.log('Somthing wrong: ' + err);
                            }else{
                                console.warn('update time');            
                            }
                        });
                    }
                }
                var theMessage = {
                    room_id: data.room_id,
                    uid: data.uid,
                    to_uid: data.to_uid,
                    message: data.message,
                    add_time: new Date(),
                    is_showtime: isShowTime(early_time),
                    msg_type: data.msg_type,
                    room_type: data.room_type,
                    fileObject: data.fileObject,
                    file_url: data.file_url,
                    option: data.option
                };
                var message = new MessageModel(theMessage);
                message.save(function(err) {
                    if (err) {
                        console.log(err);
                        return false;
                    } else {
                        console.warn('add message');
                    }
                });
                if (!result) return false;
                var addUnreadNum = function() {
                    setRooms({
                        uid: data.to_uid, 
                        to_uid: data.uid,
                        room_id: data.room_id, 
                    }, 'addNum');
                };
                if (!err && result) {
                    if (_.indexOf(result.open_rooms, data.room_id) >= 0) {
                        // 对话状态
                        socket.broadcast.to(data.room_id).emit('new_message', data);
                    } else {
                        // 未对话状态
                        addUnreadNum();
                    }
                } else {
                    addUnreadNum();
                }
            });
        });
        // 添加在线人员
        socket.on('connected', function(data) {
            var theUser = {
                uid: data.uid,
                online: 1,
                rooms: [],
                open_rooms: []
            };
            UserModel.findOne({
                uid: data.uid
            }, function(err, result) {
                if (result) {
                    UserModel.update({
                        uid: data.uid
                    }, {
                        online: 1,
                        open_rooms: []
                    }, function(err) {
                        console.warn('online');
                    });
                    theUser.rooms = result.rooms;
                } else {
                    var user = new UserModel(theUser);
                    user.save(function(err) {
                        if (err) console.log(err);
                        console.warn('add online');
                    });
                }
                socket.broadcast.emit('user_joined', theUser);
            });
        });
        // 正在打字
        socket.on('typing', function(data) {
            socket.broadcast.to(data.room_id).emit('typing');
        });
        // 停止打字
        socket.on('stop_typing', function(data) {
            socket.broadcast.to(data.room_id).emit('stop_typing');
        });
        // 断开连接
        socket.on('exit', function(data) {
            console.warn(data);
            var theUser = {
                uid: data.uid,
                online: 0
            };
            UserModel.update({
                uid: data.uid
            }, {
                online: 0,
                open_rooms: []
            }, function(err) {
                console.warn('退出登录');
            });
            socket.broadcast.emit('user_joined', theUser);
        });
    });
};
