var express = require('express'),
	router = express.Router(),
	MessageController = require('../controllers/message');

router.get('/:room_id/sort/:sort/offset/:offset/num/:num/isold/:isold', MessageController.list);

module.exports = router;