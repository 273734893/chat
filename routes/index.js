var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
	req.session.test = "fffffffffffff";
	// res.send(req.session.test);
	res.render('index', { title: 'Express' + req.session.test });
});

module.exports = router;
