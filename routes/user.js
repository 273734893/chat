var express = require('express'),
	router = express.Router(),
	UserController = require('../controllers/user');

/* GET user listing. */
router.get('/', UserController.readUser);
router.get('/save', UserController.saveUser);

module.exports = router;