/**
 * 模块依赖
 */
var express = require('express');
var http = require('http');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var mongoose = require('mongoose');
var SessionStore = require('connect-mongo')(session);
var dburl = 'mongodb://chatadmin:Px1aUmZatvZGbeas@127.0.0.1:27017/chat';
global.db = mongoose.createConnection(dburl);
var app = express();
app.usersArr = [];
app.server = http.createServer(app);
app.sessionStore = new SessionStore({
    url: dburl
});

//环境变量
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// 开发模式
// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(session({
    secret: 'hello',
    cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 1
    },
    store: app.sessionStore
}));

app.use('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By",' 3.2.1')
    res.header("Content-Type", "application/json;charset=utf-8");
    next();
});

var userRouter = require('./routes/user'),
    messageRouter = require('./routes/message');
app.use('/message', messageRouter);

var socket = require('./routes/socket')(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
// 链接错误
db.on('error', function(error) {
    console.log(error);
});

module.exports = app;
