var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var userShema = new Schema({
    uid: Number,
    online: Number,
    rooms: [{
        room_id: String,
        unread_num: Number,
        early_time: String
    }],
    open_rooms: [String]
});

module.exports = db.model('users', userShema);
