var mongoose = require('mongoose');
var sessionShema = new mongoose.Schema({
    _id: {type: String},
    session: {type: String},
    expires: {type: Date}
});

module.exports = db.model('sessions', sessionShema);
