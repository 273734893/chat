var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var messageShema = new Schema({
		room_id: String, 
		message: String,
		add_time: Date,
		is_showtime: Boolean,
		uid: Number,
		to_uid: Number,
		msg_type: String,
		room_type: Number,
        file_url: String,
        fileObject: Object
	});

// db is global
module.exports = db.model('messages', messageShema);